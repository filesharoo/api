package main

import (
	"log"
	"net/http"

	"gitlab.com/filesharoo/api/config"
	"gitlab.com/filesharoo/api/database"
	"gitlab.com/filesharoo/api/fileapi"
)

func main() {
	c := &config.Config{}
	c.DetectEnvironment()

	mongoClient := &database.MongoClient{}
	mongoClient.SetOptions(c.Mongo)

	mongoSession, err := mongoClient.CreateConnection()
	if err != nil {
		log.Fatal(err)
	}

	redisClient := &database.RedisClient{}
	redisClient.SetOptions(c.Redis)

	redisConn, err := redisClient.CreateConnection()
	if err != nil {
		log.Fatal(err)
	}

	api := fileapi.New(mongoSession, redisConn)
	if err := http.ListenAndServe(c.API.Addr, api); err != nil {
		log.Fatal(err)
	}
}

package config

import (
	"os"
	"time"

	"gitlab.com/filesharoo/api/database"
)

// APIOptions api settings
type APIOptions struct {
	Addr string
}

// Config config store
type Config struct {
	API   *APIOptions
	Mongo *database.MongoOptions
	Redis *database.RedisOptions
}

// DetectEnvironment figure out the current environment and build config
func (config *Config) DetectEnvironment() {
	env := os.Getenv("ENV")
	switch env {
	case "prod":
		fallthrough
	case "production":
		config.API = &APIOptions{Addr: ""}
		config.Mongo = &database.MongoOptions{URI: os.Getenv("FILESHAROO_MONGO_URI"), DialTimeout: time.Minute}
		config.Redis = &database.RedisOptions{URI: os.Getenv("FILESHAROOO_REDIS_URI")}
	case "dev":
		fallthrough
	case "devel":
		fallthrough
	case "development":
		config.API = &APIOptions{Addr: ""}
		config.Mongo = &database.MongoOptions{URI: os.Getenv("FILESHAROO_MONGO_URI"), DialTimeout: time.Second * 30}
		config.Redis = &database.RedisOptions{URI: os.Getenv("FILESHAROOO_REDIS_URI")}
	case "local":
		fallthrough
	default:
		config.API = &APIOptions{Addr: ":9520"}
		config.Mongo = &database.MongoOptions{URI: "localhost", DialTimeout: time.Second * 10}
		config.Redis = &database.RedisOptions{URI: "redis://localhost:6379"}
	}
}

package database

import (
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// HashData hashes
type HashData struct {
	ExpiresAt time.Time
	Hash      string
	ID        bson.ObjectId `bson:"_id,omitempty" json:"id"`
}

// LogData backup data
type LogData struct {
	ExpiresAt time.Time
	Hash      string
	ID        bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Name      string
}

// Query MongoDB Query
type Query map[string]interface{}

// MongoOptions options for mongodb
type MongoOptions struct {
	URI         string
	DialTimeout time.Duration
}

// MongoClient mongodb session struct
type MongoClient struct {
	session *mgo.Session
	options *MongoOptions
}

// CreateConnection open a new mongodb session
func (client *MongoClient) CreateConnection() (session *mgo.Session, err error) {
	session, err = mgo.DialWithTimeout(client.options.URI, client.options.DialTimeout)
	client.session = session
	return
}

// SetOptions set option for MongoDb client
func (client *MongoClient) SetOptions(options *MongoOptions) {
	client.options = options
}

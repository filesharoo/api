package database

import "github.com/garyburd/redigo/redis"

// RedisOptions redis config
type RedisOptions struct {
	URI string
}

// RedisClient redis conn struct
type RedisClient struct {
	conn    *redis.Conn
	options *RedisOptions
}

// CreateConnection open a new mongodb session
func (client RedisClient) CreateConnection() (conn redis.Conn, err error) {
	conn, err = redis.DialURL(client.options.URI)
	client.conn = &conn
	return
}

// SetOptions set option for MongoDb client
func (client *RedisClient) SetOptions(options *RedisOptions) {
	client.options = options
}

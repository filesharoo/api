package fileapi

import (
	"net/http"

	mgo "gopkg.in/mgo.v2"

	"github.com/garyburd/redigo/redis"
)

// New create new mux
func New(mongoSession *mgo.Session, redisConn redis.Conn) (mux *http.ServeMux) {
	mux = http.NewServeMux()
	mongo := &MongoResource{HashCollection: "hashes", LogCollection: "logs", DB: "filesharoo", Session: mongoSession}
	api := &API{Mongo: mongo, RedisConn: redisConn}
	mux.HandleFunc("/files/", api.DownloadHandler)
	mux.HandleFunc("/api/pre-upload", api.PreUploadHandler)
	mux.HandleFunc("/api/upload", api.UploadHandler)
	return
}

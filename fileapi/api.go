package fileapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/filesharoo/api/database"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/cespare/xxhash"
	"github.com/garyburd/redigo/redis"
)

var filePathPattern = regexp.MustCompile(`^/files/([a-f0-9]{24}$)`)

const (
	maxSize = 2 * 1024 * 1024
)

// MongoResource struct
type MongoResource struct {
	DB             string
	HashCollection string
	HashData       *database.HashData
	LogCollection  string
	LogData        *database.LogData
	Session        *mgo.Session
}

// API helpers for api
type API struct {
	Mongo     *MongoResource
	RedisConn redis.Conn
}

// CheckHashRequest request body to check if file is already available
type CheckHashRequest struct {
	Hash string `json:"hash"`
	Name string `json:"name"`
}

// UploadResponseData data for UploadHandler response
type UploadResponseData struct {
	ExpiresAt time.Time `json:"expiresAt"`
	Hash      string    `json:"hash"`
	URI       string    `json:"uri"`
}

// UploadResponse struct for upload handler response
type UploadResponse struct {
	Data  *UploadResponseData `json:"data"`
	Error string              `json:"error"`
}

// HandleError writes error to response
func HandleError(err error, code int, w http.ResponseWriter) {
	w.WriteHeader(code)
	message, _ := json.Marshal(&UploadResponse{Error: err.Error()})
	fmt.Fprintf(w, "%s", message)
	return
}

// save data in redis
func (api *API) addToRedis(expiresAt time.Time, hash string, id bson.ObjectId, name string) (statusCode int, err error) {
	value := hash + ":" + name
	key := "uri:" + id.Hex()

	if err = api.RedisConn.Send("MULTI"); err != nil {
		statusCode = http.StatusInternalServerError
		return
	}

	if err = api.RedisConn.Send("SETNX", key, value); err != nil {
		statusCode = http.StatusInternalServerError
		return
	}

	if err = api.RedisConn.Send("EXPIREAT", key, expiresAt.Unix()); err != nil {
		statusCode = http.StatusInternalServerError
		return
	}

	succeeded, err := api.RedisConn.Do("EXEC")
	if err != nil {
		statusCode = http.StatusInternalServerError
		return
	}
	succeededSlice := succeeded.([]interface{})

	if succeededSlice[0].(int64) != 1 && succeededSlice[1].(int64) != 1 {
		err = errors.New("file " + id.Hex() + " already present")
		statusCode = http.StatusConflict
		return
	}
	return
}

func (api *API) checkMongoForHash(hash string) (hashData *database.HashData, err error) {
	s := api.Mongo.Session.Copy()
	defer s.Close()

	hashData = &database.HashData{}
	if err = s.DB(api.Mongo.DB).C(api.Mongo.HashCollection).Find(bson.M{"hash": hash}).One(&hashData); err != nil {
		return
	}
	return
}

func (api *API) updateMongoHashExpiresAt(id bson.ObjectId, expiresAt time.Time) (err error) {
	s := api.Mongo.Session.Copy()
	defer s.Close()

	// update hash data in mongo
	query := bson.M{"$set": bson.M{"expiresAt": expiresAt}}
	if err = s.DB(api.Mongo.DB).C(api.Mongo.HashCollection).UpdateId(id, query); err != nil {
		return
	}
	return
}

// DownloadHandler download a file
func (api *API) DownloadHandler(w http.ResponseWriter, r *http.Request) {
	fileDirectory := os.Getenv("FILESHAROO_FILE_DIRECTORY")
	if fileDirectory == "" {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	matches := filePathPattern.FindStringSubmatch(strings.ToLower(r.URL.Path))
	if len(matches) != 2 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	key := "uri:" + matches[1]
	value, err := redis.String(api.RedisConn.Do("GET", key))
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	hashAndName := strings.SplitN(value, ":", 2)
	if len(hashAndName) != 2 {
		w.WriteHeader(http.StatusExpectationFailed)
		return
	}
	hash := hashAndName[0]
	name := hashAndName[1]

	w.Header().Set("Content-Disposition", "attachment; filename="+name)
	http.ServeFile(w, r, fileDirectory+hash)
	return
}

// PreUploadHandler check if file needs to be uploaded
func (api *API) PreUploadHandler(w http.ResponseWriter, r *http.Request) {
	s := api.Mongo.Session.Copy()
	defer s.Close()

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	body := &CheckHashRequest{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		HandleError(err, http.StatusBadRequest, w)
		return
	}
	if body.Hash == "" || body.Name == "" {
		err := errors.New("hash or name not present")
		HandleError(err, http.StatusPreconditionFailed, w)
		return
	}

	// check mongo for hash
	hashData, err := api.checkMongoForHash(body.Hash)
	if err != nil {
		HandleError(err, http.StatusNotFound, w)
		return
	}

	id := bson.NewObjectId()
	expiresAt := time.Now().Add(time.Hour * 24)

	// save data in redis
	if statusCode, err := api.addToRedis(expiresAt, body.Hash, id, body.Name); err != nil {
		HandleError(err, statusCode, w)
		return
	}

	// save log data in mongo
	logData := &database.LogData{ExpiresAt: expiresAt, Hash: body.Hash, ID: id, Name: body.Name}
	if err := s.DB(api.Mongo.DB).C(api.Mongo.LogCollection).Insert(logData); err != nil {
		HandleError(err, http.StatusInternalServerError, w)
		return
	}

	if expiresAt.After(hashData.ExpiresAt) {
		if err = api.updateMongoHashExpiresAt(hashData.ID, expiresAt); err != nil {
			HandleError(err, http.StatusInternalServerError, w)
			return
		}
	}

	// send response
	responseData := &UploadResponseData{ExpiresAt: expiresAt, Hash: body.Hash, URI: id.Hex()}
	response, _ := json.Marshal(&UploadResponse{Data: responseData})
	fmt.Fprintf(w, "%s", response)
	return
}

// UploadHandler upload a file
func (api *API) UploadHandler(w http.ResponseWriter, r *http.Request) {
	s := api.Mongo.Session.Copy()
	defer s.Close()

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	fileDirectory := os.Getenv("FILESHAROO_FILE_DIRECTORY")
	if fileDirectory == "" {
		err := errors.New("FILESHAROO_FILE_DIRECTORY is not set")
		HandleError(err, http.StatusInternalServerError, w)
		return
	}

	// parse multipart
	if err := r.ParseMultipartForm(5 * 1024 * 1024); err != nil {
		HandleError(err, http.StatusPreconditionFailed, w)
		return
	}
	file, handler, err := r.FormFile("file")
	if err != nil {
		HandleError(err, http.StatusUnprocessableEntity, w)
		return
	}
	defer file.Close()

	if handler.Size > maxSize {
		err := errors.New("file size exceeds 2MiB")
		HandleError(err, http.StatusInsufficientStorage, w)
		return
	}

	// convert file to bytes
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		HandleError(err, http.StatusInternalServerError, w)
		return
	}

	// hash file
	hasher := xxhash.New()
	if _, err = hasher.Write(fileBytes); err != nil {
		HandleError(err, http.StatusInternalServerError, w)
		return
	}
	hash := strconv.FormatUint(hasher.Sum64(), 16)

	id := bson.NewObjectId()
	expiresAt := time.Now().Add(time.Hour * 24)

	// save data in redis
	if statusCode, err := api.addToRedis(expiresAt, hash, id, handler.Filename); err != nil {
		HandleError(err, statusCode, w)
		return
	}

	// save log data in mongo
	logData := &database.LogData{ExpiresAt: expiresAt, Hash: hash, ID: id, Name: handler.Filename}
	if err = s.DB(api.Mongo.DB).C(api.Mongo.LogCollection).Insert(logData); err != nil {
		HandleError(err, http.StatusInternalServerError, w)
		return
	}

	hashData, err := api.checkMongoForHash(hash)
	if err != nil {
		// save file
		if err := ioutil.WriteFile(fileDirectory+hash, fileBytes, 0666); err != nil {
			HandleError(err, http.StatusInternalServerError, w)
			return
		}

		// save hash data in mongo
		hashData = &database.HashData{ExpiresAt: expiresAt, Hash: hash, ID: bson.NewObjectId()}
		if err = s.DB(api.Mongo.DB).C(api.Mongo.HashCollection).Insert(hashData); err != nil {
			HandleError(err, http.StatusInternalServerError, w)
			return
		}
	} else if err = api.updateMongoHashExpiresAt(hashData.ID, expiresAt); err != nil {
		HandleError(err, http.StatusInternalServerError, w)
		return
	}

	// send response
	responseData := &UploadResponseData{ExpiresAt: expiresAt, Hash: hash, URI: id.Hex()}
	response, _ := json.Marshal(&UploadResponse{Data: responseData})
	fmt.Fprintf(w, "%s", response)
	return
}

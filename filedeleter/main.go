package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/filesharoo/api/config"
	"gitlab.com/filesharoo/api/database"
)

func deleteFiles(session *mgo.Session) (err error) {
	s := session.Copy()
	defer s.Close()

	hashCollection := s.DB("filesharoo").C("hashes")

	fileDirectory := os.Getenv("FILESHAROO_FILE_DIRECTORY")
	if fileDirectory == "" {
		err = errors.New("FILESHAROO_FILE_DIRECTORY is not set")
		return
	}

	s.SetMode(mgo.Monotonic, true)

	query := &bson.M{"expiresat": &bson.M{"$lte": time.Now()}}
	iter := hashCollection.Find(query).Iter()

	hashData := &database.HashData{}
	for {
		fetched := iter.Next(&hashData)
		if fetched == false {
			err := iter.Err()
			if err == nil {
				break
			}
			fmt.Println(err.Error())
			continue
		}
		if err := os.Remove(fileDirectory + hashData.Hash); err != nil {
			fmt.Println(err.Error())
			continue
		}
	}

	if _, err = hashCollection.RemoveAll(query); err != nil {
		return
	}

	return
}

func main() {
	c := &config.Config{}
	c.DetectEnvironment()

	mongoClient := &database.MongoClient{}
	mongoClient.SetOptions(c.Mongo)

	mongoSession, err := mongoClient.CreateConnection()
	if err != nil {
		log.Fatal(err)
	}

	if err := deleteFiles(mongoSession); err != nil {
		log.Fatal(err)
	}
	fmt.Println("done")
}
